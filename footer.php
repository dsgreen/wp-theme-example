<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
<footer class="site-footer" role="contentinfo">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 footer-client-copy">
        <p>Copyright &copy; 2018 Client.</p>
      </div>
      <div class="col-md-6 text-md-right footer-client-links d-print-none">
        <p><a href="https://example.com/privacy" target="_blank">Privacy</a> / <a
              href="https://example.com/copyright" target="_blank">Copyright</a> / <a
              href="tel:+0000000000">000.000.0000</a> / <a href="http://example.com">example.com</a></p>
      </div>
    </div>
    <div class="row row-footer-social-links d-print-none">
      <div class="col-12 text-md-center footer-social-links">
        <p>Explore ...
          <a href="https://www.instagram.com/"><i class="fa fa-instagram" aria-hidden="true"></i> #hashtag</a>
          <a href="https://twitter.com/"><i class="fa fa-twitter" aria-hidden="true"></i> #hashtag</a>
      </div>
    </div>
  </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
