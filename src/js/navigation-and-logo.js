/*
 Main Navigation
 */

jQuery(document).ready(function($) {

  $('.container-navigation').hide();
  $('.container-navigation').removeClass('hidden');

  // toggle button
  $('.nav-toggle').click(function(){
    $('.container-navigation').slideToggle('fast');
  });

  // start logo animation
  init();

});
