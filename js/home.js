jQuery(document).ready(function($) {
  $("a.scroll-link").click(function( e ) {
    var target = '#' + this.hash.substr(1);
    var destination = $(target);
    $(document).scrollTo(
      destination, 1000, {axis:'y'}
    );
  });
});