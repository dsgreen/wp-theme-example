jQuery(document).ready(function($) {
    $(".container-navigation").hide();
    $(".container-navigation").removeClass("hidden");
    $(".nav-toggle").click(function() {
        $(".container-navigation").slideToggle("fast");
    });
    init();
});

if (!Modernizr.svg) {
    $('img[src$=".svg"]').each(function() {
        $(this).attr("src", $(this).attr("src").replace(".svg", ".png"));
    });
}