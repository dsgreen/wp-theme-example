// sub pg slider and overlay
jQuery(document).ready(function($) {
  $(".royalSlider").royalSlider({
    keyboardNavEnabled: true,
    imageScaleMode: 'fill',
    controlNavigation:'bullets',
    arrowsNav:true,
    arrowsNavAutoHide:true,
    arrowsNavHideOnTouch:true,
    addActiveClass:true
  });

  // accessible slider bullets
  $('.rsNav').children().attr('tabindex', 0).attr('role', 'button').on('keyup', function(e) {
    // enter key
    if(e.which == 13) {
      e.preventDefault()
      $('.royalSlider').data('royalSlider').goTo( $(this).index() );
    }
  });
  // move focus indicator with mouse click
  $('.rsBullet').click(function() {
    $(this).focus();
  });

  //
  $('.mp-image-trigger').magnificPopup({
    type: 'inline',
    fixedContentPos: true,
    autoFocusLast: false,
    removalDelay: 300,
    mainClass: 'mfp-fade'
  });

  $("a.scroll-link").click(function( e ) {
    var target = '#' + this.hash.substr(1);
    var destination = $(target);
    $('.mfp-wrap').scrollTo(
      destination, 1000, {axis:'y'}
    );
  });
});