<?php
/**
 *
 * Template Name: Home Page
 * Home page template.
 *
 * @package _s
 */

get_header('home');
?>
<?php while ( have_posts() ) : the_post(); ?>
<?php
	$main_image = get_field('main_image');
?>
<section class="section__featured-image--home">
  <div class="container-fluid">
    <div class="row row align-items-start">
      <div class="col-md-12 col-lg-9">
        <img src="<?php echo $main_image['url']; ?>" class="img-fluid home-lead" alt="<?php echo $main_image['alt'] != '' ? $main_image['alt'] : $main_image['title']; ?>">
      </div>
      <div class="col-md-12 col-lg-3 pt-2">
        <span class="scroll-indicator scroll-indicator--home d-none d-lg-block">
          <a href="#content" class="scroll-link" title="scroll for more">
            <i class="fa fa-angle-down fa-2x"></i></a>
        </span>
      </div>
    </div>
  </div>
</section>
<section>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 col-lg-9">
        <small class="image-grid__caption featured-image-caption--home">
          Detail of <?php echo $main_image['description']; ?>
        </small>
      </div>
    </div>
  </div>
</section>

<section class="section__text--home" id="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 offset-md-3">
        <?php the_field('text_content_1'); ?>
      </div>
    </div>
  </div>
</section>

<section class="full-width image-grid image-grid--home">
  <div class="container-fluid image-grid__container">
    <div class="row">
<?php
if( have_rows('filmstrip_cols' ) ) :
  while ( have_rows('filmstrip_cols' ) ) : the_row();
    $page = get_sub_field( 'page', false );
    $image = get_sub_field('image');
?>
      <div class="col-md-4">
        <p class="home-page__section-title first"><?php echo get_the_title($page); ?></p>
        <a href="<?php echo get_the_permalink($page); ?>">
          <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-fluid"></a>
      </div>
<?php
  endwhile;
else:
	// no rows found
endif; ?>
    </div>
  </div>
</section>

<section class="section__text--home">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 offset-md-3">
        <?php the_field('text_content_2'); ?>
      </div>
    </div>
  </div>
</section>

<section class="section__text--home conclusion--home">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 offset-md-3">
        <?php the_field('exhibition_conclusion'); ?>
      </div>
    </div>
  </div>
</section>
<?php endwhile; // End of the loop. ?>
<?php get_footer();
?>
