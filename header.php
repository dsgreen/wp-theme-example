<?php
/**
 * The header for exhibition and other pages
 *
 * This is the template that displays all of the <head> section and main content div and a grid row
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
</head>
<body <?php body_class('sub'); ?>>
<a class="sr-only sr-only-focusable" href="#content"><?php esc_html_e( 'Skip to main content', '_s' ); ?></a>

<header class="site-header header-sub-page" id="top" role="banner">
  <div class="container-fluid header-sub-page__container">
    <div class="row">
      <div class="col-md-9 header-sub-page__col-logo">
        <div class="row align-items-end justify-content-start no-gutters">
          <div class="col-sm-12 col-lg-6 col-site-brand-override">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="site-brand" title="Project">
              <div id="animate_wrapper" class="d-print-none">
                <div id="animation_container" class="animation-container--sub">
                  <canvas id="canvas" class="animation-canvas--sub" width="460" height="58">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/Project-logo.svg" class="site-logo" alt="Project">
                  </canvas>
                  <div id="dom_overlay_container" class="dom-overlay-container--sub"></div>
                </div>
                <div id="_preload_div_" class="preloader--sub">
                </div>
              </div>
              <div class="no-canvas-mobile--sub d-print-inline-block" aria-hidden="true">
                <img src="<?php echo get_template_directory_uri(); ?>/images/Project-logo.svg" class="site-logo" alt="Project">
              </div>
            </a>
          </div>
          <div class="col-sm-12 col-lg-6 col-site-title-override">
            <span class="site-exhibition-title site-exhibition-title--sub">Project</span>
          </div>
        </div>
      </div>
      <div class="col-md-3 header-sub-page__col-menu">
        <button class="nav-toggle d-print-none" tabindex="0">
          <div class="nav-wrap" tabindex="-1">
            <span class="nav-toggle-bar" aria-hidden="true"></span>
            <span class="nav-toggle-bar" aria-hidden="true"></span>
            <span class="nav-toggle-bar" aria-hidden="true"></span>
            <span class="nav-toggle-text"><?php esc_html_e( 'Menu', '_s' ); ?></span>
          </div>
        </button>
      </div>
    </div>
  </div>
</header>
<section class="d-print-none">
  <div class="container-fluid header-sub-page__container container-navigation hidden">
    <nav class="main-navigation" role="navigation">
	    <?php wp_nav_menu( array(
			    'theme_location'    => 'primary',
			    'menu'              => 'primary',
			    'depth'             => 1,
			    'container'         => '',
  		    'menu_class'        => '',
	  	    'menu_id'           => '',
		      'fallback_cb'       => false
	    )); ?>
    </nav>
  </div>
</section>
