<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package _s
 */

get_header(); ?>
		<main class="site-main" id="main" role="main">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-8 offset-md-2">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'Error 404. Page not found.', '_s' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php esc_html_e( 'Nothing was found at this location. Please try one of the following links.', '_s' ); ?></p>

          <nav class="" role="navigation" style="margin-bottom: 2em;">
						<?php wp_nav_menu( array(
							'theme_location'    => 'primary',
							'menu'              => 'primary',
							'depth'             => 1,
							'container'         => '',
							'menu_class'        => '',
							'menu_id'           => '',
							'fallback_cb'       => false
						)); ?>
          </nav>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->
          </div>
        </div>
      </div>
		</main><!-- #main -->
<?php
get_footer();
