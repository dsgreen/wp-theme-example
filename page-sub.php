<?php
/**
 *
 * Template Name: Sub Section
 * Main template for each sub section.
 *
 * @package _s
 */

get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
<!-- section text & slider -->
<section class="full-width full-width-featured" id="content">
  <div class="container-fluid full-width-featured__container">
    <div class="row">
      <div class="col-lg-4 full-width-featured__col-text">
	      <?php the_title( '<h2 class="sub-page-title">', '</h2>' ); ?>
	      <?php the_content(); ?>
      </div>
      <div class="col-lg-8 full-width-featured__col-image">
        <div class="royalSlider rsDefault">
	        <?php
	        $images = get_field('gallery');
	        if( $images ):
		        foreach( $images as $image ):
			        ?>
              <img src="<?php echo $image['url']; ?>" class="img-fluid rsImg" alt="<?php echo $image['alt'] != '' ? $image['alt'] : $image['title']; ?>">
		        <?php
		        endforeach;
	        endif;
	        ?>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- grid -->
<section class="full-width image-grid">
  <div class="container-fluid image-grid__container">
    <div class="row">
<?php
if( have_rows('grid_images' ) ) :
  $i = 0;
  while ( have_rows('grid_images' ) ) : the_row();
    //echo $i;
    $img = get_sub_field( 'grid_image' );
      // for pages other than the 4 col page (ID = 4), offset the first column
      ?>
      <div class="<?php if ($i == 0 && get_the_ID() != 4) {echo "col-md-3 offset-md-1_5";} else {echo "col-md-3";} ?> image-grid-col">
        <a href="#<?php echo $img['title']; ?>" class="mp-image-trigger">
          <img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>" class="img-fluid">
        </a>
        <small class="image-grid__caption">
          <?php
          echo $img['caption'];
          ?>
        </small>
      </div>
      <?php
      $i++;
      endwhile;
else:
    // no rows found
endif; ?>
    </div>
  </div>
</section>
<!-- overlays -->
<?php
if( have_rows('overlay' ) ) :
  while ( have_rows('overlay' ) ) : the_row();
    $imgLg = get_sub_field( 'image_lg' );
    $layout = get_sub_field('layout_switch' );

    switch ($layout) {
     case 'condensed':
?>
<section class="image-overlay mfp-hide" id="<?php echo $imgLg['title']; ?>">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6">
        <img src="<?php echo $imgLg['url']; ?>" alt="<?php echo $imgLg['alt']; ?>" class="img-fluid">
        <small class="image-overlay__caption">
          <?php echo $imgLg['description']; ?>
        </small>
      </div>
      <div class="col-md-5">
        <?php the_sub_field('main_content'); ?>
      </div>
    </div>
  </div>
</section>
<?php
      case 'condensed-2' :
?>
<section class="image-overlay mfp-hide" id="<?php echo $imgLg['title']; ?>">
 <div class="container-fluid">
   <div class="row">
     <div class="col-md-4 offset-md-2">
       <img src="<?php echo $imgLg['url']; ?>" alt="<?php echo $imgLg['alt']; ?>" class="img-fluid">
       <small class="image-overlay__caption">
         <?php echo $imgLg['description']; ?>
       </small>
     </div>
     <div class="col-md-4">
       <?php the_sub_field('main_content'); ?>
     </div>
   </div>
 </div>
</section>
<?php
      case 'two-col-offset' :
?>
<section class="image-overlay mfp-hide" id="<?php echo $imgLg['title']; ?>">
  <div class="container-fluid">
    <div class="row align-items-center">
      <div class="col-md-6 offset-md-1">
        <img src="<?php echo $imgLg['url']; ?>" alt="<?php echo $imgLg['alt']; ?>" class="img-fluid">
        <small class="image-overlay__caption">
          <?php echo $imgLg['description']; ?>
        </small>
      </div>
      <div class="col-md-4">
        <div class="image-overlay__text-side-wrap">
          <?php the_sub_field('top_content'); ?>
          <span class="scroll-indicator d-none d-md-block">
            <a href="#<?php echo $imgLg['title']; ?>-content" class="scroll-link" title="scroll for more"><i class="fa fa-angle-down fa-2x"></i></a>
          </span>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-7 offset-md-3 image-overlay__text-content-wrap" id="<?php echo $imgLg['title']; ?>-content">
        <?php the_sub_field('main_content'); ?>
      </div>
    </div>
  </div>
</section>
<?php
      case 'centered' :
?>
<section class="image-overlay mfp-hide" id="<?php echo $imgLg['title']; ?>">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <img src="<?php echo $imgLg['url']; ?>" alt="<?php echo $imgLg['alt']; ?>" class="img-fluid">
        <small class="image-overlay__caption">
          <?php echo $imgLg['description']; ?>
        </small>
        <span class="scroll-indicator d-none d-md-block">
          <a href="#<?php echo $imgLg['title']; ?>-content" class="scroll-link" title="scroll for more"><i class="fa fa-angle-down fa-2x"></i></a>
        </span>
        <div class="row">
          <div class="col-md-9 offset-md-3 image-overlay__text-content-wrap" id="<?php echo $imgLg['title']; ?>-content">
            <?php the_sub_field('main_content'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
      case 'two-col' :
?>
<section class="image-overlay mfp-hide" id="<?php echo $imgLg['title']; ?>">
  <div class="container-fluid">
    <div class="row align-items-center">
      <div class="col-md-7 offset-md-0">
        <img src="<?php echo $imgLg['url']; ?>" alt="<?php echo $imgLg['alt']; ?>" class="img-fluid">
        <small class="image-overlay__caption">
		      <?php echo $imgLg['description']; ?>
        </small>
      </div>
      <div class="col-md-4">
        <div class="image-overlay__text-side-wrap">
	        <?php the_sub_field('top_content'); ?>
          <span class="scroll-indicator d-none d-md-block">
            <a href="#<?php echo $imgLg['title']; ?>-content" class="scroll-link" title="scroll for more"><i class="fa fa-angle-down fa-2x"></i></a>
          </span>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-7 offset-md-3 image-overlay__text-content-wrap" id="<?php echo $imgLg['title']; ?>-content">
	      <?php the_sub_field('main_content'); ?>
      </div>
    </div>
  </div>
</section>
<?php
    } // end switch statement
  
  endwhile;
else:
	// no rows found
endif; ?>

<section class="conclusion">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 offset-md-3">
        <?php the_field('more_text'); ?>
      </div>
    </div>
  </div>
</section>

<?php endwhile; // End of the loop. ?>
<?php get_footer(); ?>
