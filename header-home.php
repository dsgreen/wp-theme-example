<?php
/**
 * The header for the home page.
 *
 * This is the template that displays all of the <head> section and main content div and a grid row
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
</head>
<body <?php body_class('home'); ?>>
<a class="sr-only sr-only-focusable" href="#content"><?php esc_html_e( 'Skip to main content', '_s' ); ?></a>

<section class="client-banner" id="top" role="banner">
  <div class="container-fluid">
    <a href="http://example.com" class="client-brand">
      <img src="<?php echo get_template_directory_uri(); ?>/images/client-logo.png" class="client-logo" alt="Client">
    </a>
  </div>
</section>

<header class="site-header header-home-page" role="banner">
  <div class="container-fluid header-home-page__container">
    <div class="row">
      <div class="col-lg-9 text-center text-lg-right">
        <div class="header-home-page__col-logo">
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="site-brand" title="Project">
            <div id="animation_container" class="animation-container--home d-print-none">
              <canvas id="canvas" class="animation-canvas--home" width="670" height="130">
                <img src="<?php echo get_template_directory_uri(); ?>/images/Project-logo.svg" class="site-logo--home" alt="Project">
              </canvas>
              <div id="dom_overlay_container" class="dom-overlay-container--home"></div>
              <div id="_preload_div_" class="preloader--home">
              </div>
            </div>
            <div class="no-canvas-mobile--home d-print-inline-block" aria-hidden="true">
              <img src="<?php echo get_template_directory_uri(); ?>/images/Project-logo.svg" class="site-logo--home" alt="Project">
            </div>
          </a>
        </div>
      </div>
      <div class="col-12 col-lg-3 text-center text-lg-left header-home-page__col-menu">
        <button class="nav-toggle nav-toggle--home d-print-none" tabindex="0">
          <div class="nav-wrap" tabindex="-1">
            <span class="nav-toggle-bar" aria-hidden="true"></span>
            <span class="nav-toggle-bar" aria-hidden="true"></span>
            <span class="nav-toggle-bar" aria-hidden="true"></span>
            <span class="nav-toggle-text"><?php esc_html_e( 'Menu', '_s' ); ?></span>
          </div>
        </button>
      </div>
    </div>
  </div>
</header>

<section class="d-print-none">
  <div class="container-fluid container-navigation container-navigation--home">
    <nav class="main-navigation" role="navigation">
      <?php wp_nav_menu( array(
        'theme_location'    => 'primary',
        'menu'              => 'primary',
        'depth'             => 1,
        'container'         => '',
        'menu_class'        => '',
        'menu_id'           => '',
        'fallback_cb'       => false
      )); ?>
    </nav>
  </div>
</section>
